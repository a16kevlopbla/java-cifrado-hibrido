package activitats;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.Arrays;
import java.util.Scanner;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

public class LopKevA04RSAAES {

	public static void main(String[] args) throws NoSuchAlgorithmException, KeyStoreException, CertificateException,
			IOException, UnrecoverableKeyException {
		Scanner in = new Scanner(System.in);
		String text, answer, keyStoreName, keyStorePass, aliasCertification = "", aliasKey = "", keyPass = "";
		int cMode = 0;
		SecretKey sKey;
		byte[] bText, cText, kByte, ckByte;
		boolean exit = false;

		do {
			System.out.println("Escribe el texto a cifrar/descifrar:");
			text = in.nextLine();
			System.out.println("Selecciona:");
			System.out.println("1. Cifrar.");
			System.out.println("2. Descifrar.");
			answer = in.nextLine();
			if (answer.equals("1")) {
				cMode = Cipher.ENCRYPT_MODE;
				bText = text.getBytes();
			} else {
				cMode = Cipher.DECRYPT_MODE;
				bText = DatatypeConverter.parseHexBinary(text);
			}

			System.out.println("Escribe el almacen de llaves a utilizar:");
			keyStoreName = in.nextLine();
			System.out.println("Escribe la contraseña del almacen de llaves:");
			keyStorePass = in.nextLine();

			if (cMode == Cipher.ENCRYPT_MODE) {
				System.out.println("Escribe el alias del certificado a utilizar:");
				aliasCertification = in.nextLine();
			} else if (cMode == Cipher.DECRYPT_MODE) {
				System.out.println("Escribe el alias de la llave del almacen:");
				aliasKey = in.nextLine();
				System.out.println("Escribe la contraseña de la llave del almacen:");
				keyPass = in.nextLine();
			}

			KeyStore keyStore = KeyStore.getInstance("JKS");
			File f = new File(keyStoreName);
			if (f.isFile()) {
				FileInputStream fin = new FileInputStream(f);
				keyStore.load(fin, keyStorePass.toCharArray());
			}

			if (cMode == Cipher.ENCRYPT_MODE) {
				KeyGenerator kgen = KeyGenerator.getInstance("AES");
				kgen.init(192);
				sKey = kgen.generateKey();

				cText = transformation(cMode, sKey, bText, "AES/CBC/PKCS5Padding");

				PublicKey publicKey = keyStore.getCertificate(aliasCertification).getPublicKey();
				kByte = sKey.getEncoded();

				ckByte = transformation(cMode, publicKey, kByte, "RSA/ECB/PKCS1Padding");

				System.out.println("Llave + Texto cifrado: " + DatatypeConverter.printHexBinary(ckByte)
						+ DatatypeConverter.printHexBinary(cText));
			} else if (cMode == Cipher.DECRYPT_MODE) {
				ckByte = Arrays.copyOfRange(bText, 0, 256);
				cText = Arrays.copyOfRange(bText, 256, bText.length);

				PrivateKey privateKey = (PrivateKey) keyStore.getKey(aliasKey, keyPass.toCharArray());

				kByte = transformation(cMode, privateKey, ckByte, "RSA/ECB/PKCS1Padding");

				SecretKey skey = new SecretKeySpec(kByte, "AES");

				bText = transformation(cMode, skey, cText, "AES/CBC/PKCS5Padding");

				System.out.println("Texto sin cifrar: " + new String(bText));
			}
		} while (!exit);
		in.close();
	}

	public static byte[] transformation(int cMode, Key key, byte[] bTexto, String cipheringMethod) {
		final byte[] IV_PARAM_128 = { 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C,
				0x0D, 0x0E, 0x0F };
		try {
			if (cipheringMethod.contains("AES")) {
				Cipher c;
				c = Cipher.getInstance(cipheringMethod);
				IvParameterSpec iv = new IvParameterSpec(IV_PARAM_128);
				c.init(cMode, key, iv);
				return c.doFinal(bTexto);
			} else if (cipheringMethod.contains("RSA")) {
				Cipher c;
				c = Cipher.getInstance(cipheringMethod);
				c.init(cMode, key);
				return c.doFinal(bTexto);
			}
		} catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
			e.printStackTrace();
		} catch (InvalidKeyException e) {
			e.printStackTrace();
		} catch (InvalidAlgorithmParameterException e) {
			e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			e.printStackTrace();
		} catch (BadPaddingException e) {
			e.printStackTrace();
		}
		return null;
	}
}
